#include <iostream>
using namespace std;

class Animal {
private:
    string animalSound;

public:
    Animal() { animalSound = "Aooooo!"; }
    
    virtual void Voice() {
        cout << animalSound;
    }
};

class Dog : public Animal {
private:
    string dogSound = "Woof!";

public:

    void Voice() override {
        cout << dogSound;
    }

};

class Tiger : public Animal {
private:
    string tigerSound = "Chuff!";

public:

    void Voice() override {
        cout << tigerSound;
    }
};

class Parrot : public Animal {
private:
    string parrotSound = "pik-a-boo!";
public:
 
    void Voice() override {
        cout << parrotSound;
    }
};

int main()
{ 
    Animal* p[4] = { new Animal, new Parrot, new Dog, new Tiger };

    for (int i = 0; i < 4; i++)
    {
        p[i] -> Voice();
    }

}